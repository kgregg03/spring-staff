package com.citi.training.staff.service;

import com.citi.training.staff.model.Employee;

import org.junit.jupiter.api.Test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


@SpringBootTest
public class EmployeeServiceTests {

    private static final Logger LOG = LoggerFactory.getLogger(EmployeeServiceTests.class);

    @Autowired
    private EmployeeService employeeService;

    @Test
    public void test_save_sanityCheck() {

        LOG.debug("This is a message");

        Employee testEmployee = new Employee();
        testEmployee.setName("Mr. Test McTesty");
        testEmployee.setAddress("123 Main Street");

        employeeService.save(testEmployee);
        
    }

    @Test
    public void test_findAll_sanityCheck() {
        assert(employeeService.findAll().size() == 0);
    }
}    

