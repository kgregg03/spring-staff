package com.citi.training.staff.service;

import static org.mockito.Mockito.when;

import com.citi.training.staff.dao.EmployeeMongoRepo;
import com.citi.training.staff.model.Employee;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@SpringBootTest
public class EmployeeServiceMockingTests {

    @Autowired
    private EmployeeService employeeService;

    @MockBean
    private EmployeeMongoRepo mockEmployeeRepo;

    @Test
    public void test_employeeService_save(){
        Employee testEmployee = new Employee();
        testEmployee.setName("Mr. Test McTesty");
        testEmployee.setAddress("123 Main Street");

        // tell mockRepo that employeeService is going to call save()
        // when it does return the testEmployee object
        when(mockEmployeeRepo.save(testEmployee)).thenReturn(testEmployee);

        employeeService.save(testEmployee);
    }    
}
